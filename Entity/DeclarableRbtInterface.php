<?php

namespace Example\AsponeBundle\Entity;

interface DeclarableRbtInterface extends DeclarableInterface
{

    /**
     * @return mixed
     */
    public function get3519DH();
    public function get3519DN();
    public function get3519FK();
    public function get3519DD();
    public function get3519DI();
    public function get3519AA();

    /**
     * @return array TexteLibre[$i]
     */
    public function get3519FJ();

    public function get3519DC();

    public function get3519DG();
}